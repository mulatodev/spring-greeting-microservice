package com.arcadia.samples.greeting.controller;

import com.arcadia.samples.greeting.model.GreetingModel;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class GreetingController {

    public GreetingController(){

    }

    @GetMapping("/greeting")
    public String ShowGreeting(@RequestParam String name){

        GreetingModel greeting = new GreetingModel(name);
        return greeting.getGreeting();
    }
}
