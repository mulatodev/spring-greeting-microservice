package com.arcadia.samples.greeting.model;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class GreetingModel {

    String name = "";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    public GreetingModel(String nameRequest){
        name = nameRequest;
    }

    public String getGreeting(){

        LocalDateTime now = LocalDateTime.now();
        return "Hi " + name + " at " + dtf.format(now);
    }
}
